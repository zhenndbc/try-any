package service

import (
	"context"

	pb "oneline-exam/api/userService"
)

type UserServiceService struct {
	pb.UnimplementedUserServiceServer
}

func NewUserServiceService() *UserServiceService {
	return &UserServiceService{}
}

func (s *UserServiceService) GetUser(ctx context.Context, req *pb.UserRequest) (*pb.UserInfo, error) {
	return &pb.UserInfo{}, nil
}
func (s *UserServiceService) UpdateUser(ctx context.Context, req *pb.UserUpdateRequest) (*pb.UserResponse, error) {
	return &pb.UserResponse{}, nil
}
