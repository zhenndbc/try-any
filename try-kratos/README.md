# 使用 Protobuf 定义验证码生成接口

1. 定义 Protobuf 文件说明接口
2. 利用 protoc 基于 protobuf 生成必要代码
3. 将生成的代码整合到项目中
4. 完善业务逻辑

## 增加 proto 文件模板

命令 `kratos proto add` 添加 `.proto` 文件

```shell
kratos proto api/verifyCode/verifyCode.proto
```

1. 生成 `kraots` 项目
    ```shell
    kratos new customer
    cd customer
    go get github.com/google/wire/cmd/wire@v0.5.0
    go generate ./...
    ```

2. 创建 `.proto` 文件
    ```shell
    kratos proto add api/verifyCode/verifyCode.proto
    ```

3. 生成客户端文件
   ```shell
   kratos proto client api/verifyCode/verifyCode.proto
   ```

4. 服务端文件
   ```shell
   kratos proto server api/verifyCode/verifyCode.proto
   ```

5. 注册服务端(internal/server/server.go)
   ```go
   // ProviderSet is server providers.
   var ProviderSet = wire.NewSet(NewGRPCServer, NewHTTPServer)
   ```

6. 修改业务逻辑
      ```go
   kratos run
   ```

7. 重新生成代码
   ```shell
   go generate ./...
   ```

8. 启动
   ```shell
   kratos run
   ```

### 如何调用 grpc 服务

1. 连接目标 grpc 服务器
   ```go
   	// 1.校验邮箱是否正确
	pattern := `^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$`
	regexpPattern := regexp.MustCompile(pattern)
	if !regexpPattern.MatchString(req.Telephone) {
		return &pb.GetVerifyResp{
			Code:    1,
			Message: "电话格式错误",
		}, nil
	}
   	// 2.通过验证码服务生成验证码(服务间通信，grpc)
	// 连接 grpc 服务器
	conn, err := grpc.DialInsecure(context.Background(), 
		grpc.WithEndpoint("localhost:9000"),
		)
	if err != nil {
		return &pb.GetVerifyResp{
			Code:    1,
			Message: "验证码服务不可用",
		}, nil
	}
   ```
2. 发送获取验证码请求
   > 需要在 customer 服务中，使用 verifyCode 的 `.proto` 文件
   >
   > 生成客户端存根代码（stub代码），才能完成远程调用。
   >
   > 保持一致 copy `.proto` file
   ```go
	// 2.2,发送获取服务器代码
	client := verifyCode.NewVerifyCodeClient(conn)
	reply, err := client.GetVerifyCode(context.Background(), &verifyCode.GetVerifyCodeRequest{
		Length: 6,
		Type:   1,
	})
	if err != nil {
		return &pb.GetVerifyResp{
			Code:    1,
			Message: "验证码获取失败",
		}, nil
	}
   ```

## 临时存储验证码

> 实现思路: redis 缓存 60秒 {key: value} => {手机号码: 验证码}

```shell
# redis 临时存储
go get github.com/redis/go-redis/v9
```