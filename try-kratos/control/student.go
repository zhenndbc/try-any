package control

import (
	"fmt"
)

// 学生基本信息管理
type Student struct {
	ID      int
	Name    string
	Age     int
	Gender  string
	ClassID int
}

func (s *Student) CreateStudent() {
	fmt.Println("创建学生")
}

func (s *Student) UpdateStudent() {
	fmt.Println("更新学生")
}

func (s *Student) DeleteStudent() {
	fmt.Println("删除学生")
}

// 学生课程管理
type Course struct {
	ID        int
	Name      string
	TeacherID int
	Students  []Student
}

func (c *Course) CreateCourse() {
	fmt.Println("创建课程")
}

func (c *Course) UpdateCourse() {
	fmt.Println("更新课程")
}

func (c *Course) DeleteCourse() {
	fmt.Println("删除课程")
}

// 学生成绩管理
type Score struct {
	ID         int
	StudentID  int
	CourseID   int
	Score      float64
	CreateTime string
}

func (sc *Score) CreateScore() {
	fmt.Println("录入成绩")
}

func (sc *Score) UpdateScore() {
	fmt.Println("更新成绩")
}

func (sc *Score) DeleteScore() {
	fmt.Println("删除成绩")
}

func main() {
	student := &Student{ID: 1, Name: "张三", Age: 18, Gender: "男", ClassID: 1}
	course := &Course{ID: 1, Name: "数学", TeacherID: 1001, Students: []Student{*student}}
	score := &Score{ID: 1, StudentID: 1, CourseID: 1, Score: 95.5, CreateTime: "2022-01-01"}

	student.CreateStudent()
	course.CreateCourse()
	score.CreateScore()
}
