package control

import (
	"fmt"
)

// 教师基本信息管理
type Teacher struct {
	ID      int
	Name    string
	Courses []Course
}

func (t *Teacher) CreateTeacher() {
	fmt.Println("创建教师")
}

func (t *Teacher) UpdateTeacher() {
	fmt.Println("更新教师")
}

func (t *Teacher) DeleteTeacher() {
	fmt.Println("删除教师")
}

// 教师课程管理
type Course struct {
	ID        int
	Name      string
	TeacherID int
	Students  []Student
}

func (c *Course) CreateCourse() {
	fmt.Println("创建课程")
}

func (c *Course) UpdateCourse() {
	fmt.Println("更新课程")
}

func (c *Course) DeleteCourse() {
	fmt.Println("删除课程")
}

// 学生与课程关系管理
type Student struct {
	ID      int
	Name    string
	Courses []Course
}

func (s *Student) CreateStudent() {
	fmt.Println("创建学生")
}

func (s *Student) UpdateStudent() {
	fmt.Println("更新学生")
}

func (s *Student) DeleteStudent() {
	fmt.Println("删除学生")
}

func main() {
	teacher := &Teacher{ID: 1001, Name: "张三", Courses: []Course{}}
	course := &Course{ID: 1, Name: "数学", TeacherID: 1001, Students: []Student{}}
	student := &Student{ID: 1, Name: "李四", Courses: []Course{*course}}

	teacher.CreateTeacher()
	course.CreateCourse()
	student.CreateStudent()
}
