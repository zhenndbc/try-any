package control

import (
	"fmt"
)

// 题目分类管理
type Category struct {
	ID        int
	Name      string
	Questions []Question
}

func (c *Category) CreateCategory() {
	fmt.Println("创建题目分类")
}

func (c *Category) UpdateCategory() {
	fmt.Println("更新题目分类")
}

func (c *Category) DeleteCategory() {
	fmt.Println("删除题目分类")
}

// 题目标签管理
type Tag struct {
	ID     int
	Name   string
	Answer bool
}

func (t *Tag) CreateTag() {
	fmt.Println("创建题目标签")
}

func (t *Tag) UpdateTag() {
	fmt.Println("更新题目标签")
}

func (t *Tag) DeleteTag() {
	fmt.Println("删除题目标签")
}

// 题目基本信息管理
type Question struct {
	ID            int
	Content       string
	Options       []string
	CorrectAnswer string
	Tags          []Tag
}

func (q *Question) CreateQuestion() {
	fmt.Println("创建题目")
}

func (q *Question) UpdateQuestion() {
	fmt.Println("更新题目")
}

func (q *Question) DeleteQuestion() {
	fmt.Println("删除题目")
}

// 题目难度和分数管理
type Difficulty struct {
	ID   int
	Name string
}

type Score struct {
	ID     int
	Name   string
	Points int
}

func (d *Difficulty) CreateDifficulty() {
}

func (s *Score) CreateScore() {

}

func main() {
	category := &Category{ID: 1, Name: "数学"}
	tag := &Tag{ID: 1, Name: "选择题", Answer: true}
	question := &Question{ID: 1, Content: "1+1=?", Options: []string{"1", "2", "3"}, CorrectAnswer: "2", Tags: []Tag{*tag}}
	difficulty := &Difficulty{ID: 1, Name: "简单"}
	score := &Score{ID: 1, Name: "单选题", Points: 1}

	category.CreateCategory()
	tag.CreateTag()
	question.CreateQuestion()
	difficulty.CreateDifficulty()
	score.CreateScore()
}
