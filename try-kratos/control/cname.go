package control

import (
	"fmt"
)

// 学生基本信息管理
type Student struct {
  ID        int
  Name      string
  Scores    []Score
}

func (s *Student) CreateStudent() {
  fmt.Println("创建学生")
}

func (s *Student) UpdateStudent() {
  fmt.Println("更新学生")
}

func (s *Student) DeleteStudent() {
  fmt.Println("删除学生")
}

// 班级基本信息管理
type Class struct {
  ID        int
  Name      string
  TeacherID int
  Students  []Student
}

func (c *Class) CreateClass() {
  fmt.Println("创建班级")
}

func (c *Class) UpdateClass() {
  fmt.Println("更新班级")
}

func (c *Class) DeleteClass() {
  fmt.Println("删除班级")
}

// 学生分班管理
type Assignment struct {
  ID          int
  StudentID   int
  ClassID     int
  AssignTime  string
}

func (a *Assignment) CreateAssignment() {
  fmt.Println("分配学生到班级")
}

func (a *Assignment) UpdateAssignment() {
  fmt.Println("修改学生所属班级信息")
}

// 班级成绩统计和分析
type Score struct {
  ID          int
  StudentID   int
  ClassID     int
  Score       float64
  CreateTime  string
}

func (sc *Score) CreateScore() {
  fmt.Println("录入成绩")
}

func (sc *Score) UpdateScore() {
  fmt.Println("更新成绩")
}

func (sc *Score) DeleteScore() {
  fmt.Println("删除成绩")
}

func main() {
  student := &Student{ID: 1, Name: "张三"}
  class := &Class{ID: 1, Name: "一年级一班", TeacherID: 1001, Students: []Student{*student}}
  assignment := &Assignment{ID: 1, StudentID: 1, ClassID: 1, AssignTime: "2022-01-01"}
  score := &Score{ID: 1, StudentID: 1, ClassID: 1, Score: 95.5, CreateTime: "2022-01-01"}

  student.CreateStudent()
  class.CreateClass()
  assignment.CreateAssignment()
  score.CreateScore()
}