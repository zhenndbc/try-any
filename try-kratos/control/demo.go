package control

/*
	用户管理: {
		用户基本信息管理: [
			用户角色和权限管理,
			用户认证和授权管理
		]
	}
*/

// 用户基本信息管理
type UserInfo struct {
	UserID    string // 用户 ID
	Username  string // 用户名
	Password  string // 用户密码
	Email     string // 用户邮箱
	CreatedAt string // 创建时间
	UpdatedAt string // 更新时间
	// 用户角色
	RoleUser Role
	// 用户认证
	UserAuth Auth
}

// 用户角色和权限管理
type Role struct {
	RoleID      int
	Permissions []string
}

// 用户认证和授权管理
type Auth struct {
	UserID       int
	Token        string
	ExpiresAt    string
	RefreshToken string
}

// 用户登录
func (user *UserInfo) Login() {

}

// 用户退出
func (user *UserInfo) Logout() {

}

// 用户信息修改
func (user *UserInfo) Change() {

}

// 删除用户
func (user *UserInfo) Delete() {

}

// 按照上述格式继续


// 考试管理
type ExamManagement struct {
	ExamID      int
	ExamName    string
	StartTime   string
	EndTime     string
	Questions   []Question
	ClassroomID int
	TeacherID   int
}

// 题库管理
type QuestionBankManagement struct {
	QuestionID      int
	QuestionContent string
	Options          []Option
	Answer           string
}

// 成绩管理
type GradeManagement struct {
	GradeID      int
	StudentID    int
	ExamID       int
	Score        float64
	CreatedAt    string
	UpdatedAt    string
}

// 班级管理
type ClassManagement struct {
	ClassID     int
	ClassName   string
	Students    []StudentManagement
	TeacherID   int
}

// 学生管理
type StudentManagement struct {
	StudentID     int
	StudentName   string
	ClassID       int
	UserInfo      UserInfo
	ExamResults   []ExamResult
}

// 课程管理
type CourseManagement struct {
	CourseID     int
	CourseName   string
	TeacherID    int
	QuestionBank []QuestionBankManagement
}

// 老师管理
type TeacherManagement struct {
	TeacherID    int
	TeacherName  string
	Classes      []ClassManagement
	Courses      []CourseManagement
}

// 考试题目结构体
type Question struct {
	QuestionID      int
	QuestionContent string
	Options          []Option
	Answer           string
}

// 考试选项结构体
type Option struct {
	OptionID   int
	OptionText string
	IsCorrect  bool
}

// 学生考试结果结构体
type ExamResult struct {
	ExamID       int
	StudentID    int
	Score        float64
	CreatedAt    string
	UpdatedAt    string
}