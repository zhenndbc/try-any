# try-any

> 年轻就要拼搏，不断尝试。

## 阅览

> 每个项目都有对应的 README

### 已完成项目

<details>

<summary>vite-card</summary>

> 一个基于 vite + react + antd 构建的卡片项目
> [Goto](./vite-card/)

</details>

<details>

<summary>wails-chat</summary>

> 一个基于 react + golang + antd 构建的简易项目
> [Goto](./vite-card/)

</details>
