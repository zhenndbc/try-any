# 禁毒知识提取

## 定义

```go
type Question struct {
	Question string   `json:"question"`
	Options  []string `json:"options"`
	Answer   string   `json:"answer"`
}
```

## 数据来源

> [禁毒知识科普](http://www.nncc626.com/2023-11/14/c_1212301724.htm)

主程序

```go
func main() {
	dirs, _ := os.ReadDir("source")

	for _, childFile := range dirs {
		f := childFile.Name()

		fn := filepath.Join(`output`, strings.Split(f, ".")[0]+".json")
		file, _ := os.ReadFile(filepath.Join("source", f))

		questions, err := ExtractQuestions(string(file))
		if err != nil {
			fmt.Println("提取问题错误:", err)
			return
		}

		err = SaveQuestionsToFile(questions, fn)
		if err != nil {
			fmt.Println("保存问题到文件错误:", err)
			return
		}

		fmt.Println(childFile.Name() + "已成功保存到文件" + fn)
	}
}
```

---

## 运行

```shell
go mod tidy
go run main.go
```
