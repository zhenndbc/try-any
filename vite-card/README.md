## vite-card

> 一个基于 vite + react 的简易 demo

`项目结构`

```
vite-card
├── README.md
├── index.html
├── package.json            // 项目依赖
├── public
│   └── logo.svg
├── backend
│   ├── go.mod
│   ├── go.sum
│   └── main.go             // 后端实现
├── src
│   ├── App.css             // 基础样式
│   ├── auth
│   │   └── user.js         // 用户鉴权
│   ├── components
│   │   ├── AppLayout.jsx   // 主页组件
│   │   ├── CardDetail.jsx  // 卡片组件
│   │   └── LoginForm.jsx   // 登录组件
│   ├── main.jsx            // 入口
│   └── shared
│       ├── request.js      // 请求封装
│       └── token.js        // 浏览器存储
└── vite.config.js          // vite 配置
```

### 安装

1. 依赖
   ```shell
   npm i --registry=http://registry.npmmirror.com
   ```
2. 启动

   ```shell
   npm run dev
   ```

3. 访问
   ```shell
   http://localhost:9000
   ```

### 注意

> 后端使用 Go-Mux 模拟的 参考 [请求封装](./src/shared/request.js) 实现
>
> 项目的最大意义就是一个参考

```shell
go build . && ./vite-card.exe
```
