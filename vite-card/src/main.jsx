/* 
  程序入口
*/

import { StrictMode, useEffect, useState } from 'react'
import { createRoot } from 'react-dom/client'
import { Navigate, Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import { getToken } from './shared/token'

import AppLayout from './components/AppLayout'
import LoginForm from './components/LoginForm'


const Root = () => {
  const [AuthStatus, setAuthStatus] = useState(false)

  useEffect(() => {
    setAuthStatus(!!getToken())
  }, [])


  return (
    <Router>
      <Routes>
        <Route path="/login" element={<LoginForm />} />
        <Route
          path="/home/*"
          element={AuthStatus ? <AppLayout /> : <Navigate to="/login" replace />}
        />
      </Routes>
    </Router>
  )
}


createRoot(document
  .getElementById('root'))
  .render(
    <StrictMode>
      <Root />
    </StrictMode>
  )
