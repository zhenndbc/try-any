/* 
    登录页面
*/

import { Button, Form, Input, message } from 'antd'
import React from 'react'
import { Navigate } from 'react-router-dom'
import '../App.css'
import { userStore } from '../auth/user'
import { getToken } from '../shared/token'


const LoginForm = () => {
    if (getToken()) return <Navigate to='/home' />

    return (
        <div className="login-container">
            <h1 className="login-title">Butterfly</h1>
            <Menu onFinish={(values) => userStore.login(values)} />
        </div>
    )
}

const Menu = ({ onFinish }) => {
    return (
        <Form onFinish={onFinish}>
            <Form.Item name="username">
                <Input type="text" placeholder="用户名" required />
            </Form.Item>
            <Form.Item name="password">
                <Input type="password" placeholder="密码" required />
            </Form.Item>
            <Form.Item style={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
                <Button type="primary" htmlType="submit" style={{ marginRight: 30 }}>
                    登录
                </Button>
                <Button type="default" onClick={
                    () => message.info('跳转到注册页面')
                }>
                    注册
                </Button>
            </Form.Item>
        </Form>
    )
}


export default LoginForm
