/* 
    浏览器简易存储
*/

export function getToken () {
    return localStorage.getItem("token")
}

export function setToken (token) {
    localStorage.setItem("token", token)
}

export function removeToken () {
    localStorage.removeItem("token")
}
