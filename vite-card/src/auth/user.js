/* 
    登录鉴权
*/

import { message } from 'antd'
import { ApiClient } from '../shared/request'
import { removeToken, setToken } from '../shared/token'

export const userStore = new (class {
    user = null;

    login = ({ username, password }) => {
        ApiClient.login({ username, passwd: password, port: 50010 })
            .then(resp => {
                this.user = username
                setToken(resp.token)
                message.success(`登陆成功`)
            })
            .catch(_ => {
                message.error(`登陆失败`)
            })
    };


    logout = () => {
        try {
            this.user = null
            removeToken()
        } catch (error) {
            message.error(`Logout failed: ${error}`)
            throw error
        }
    };
})()
