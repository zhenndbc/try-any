// vite.config.js
import react from '@vitejs/plugin-react'
import { defineConfig } from 'vite'

export default defineConfig({
  plugins: [react()],
  server: {
    port: 9000, // 服务器端口
    // 代理地址
    // proxy: {
    //   '/api/v1': {
    //     target: 'http://localhost:50010', // 后端代理地址
    //     ws: false, 
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace(/^\/api\/v1/, '')
    //   },
    // },
  },
})
