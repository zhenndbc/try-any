# React + Vite + Golang

### 前端结构

```
── frontend
    ├── index.html
    ├── package.json
    ├── public
    │   └── vite.svg
    ├── src
    │   ├── assets                  // 资源路径
    │   │   ├── 404.jpg
    │   │   ├── background.jpg
    │   │   ├── chatlogo.jpg
    │   │   ├── ct.jpg
    │   │   └── react.svg
    │   ├── components
    │   │   ├── AnimatedButton.jsx // 动画按钮组件
    │   │   ├── ChatConfig.jsx     // 聊天配置组件
    │   │   ├── ChatWindow.jsx     // 聊天窗口组件
    │   │   └── FloatChat.jsx      // 悬浮窗口组件
    │   ├── css
    │   │   ├── ChatWindow.css
    │   │   ├── NotFoundPage.css
    │   │   ├── avatar.css         // 头像 CSS
    │   │   ├── demo.css           // 聊天 CSS
    │   │   └── index.css
    │   ├── main.jsx               // 程序入口
    │   ├── routes
    │   │   ├── base
    │   │   │   ├── BasePage.jsx   // 基本页面
    │   │   │   ├── CodePage.jsx   // 代码页面
    │   │   │   ├── ConfigPage.jsx // 配置页面
    │   │   │   ├── IndexPage.jsx  // 管理页面
    │   │   │   ├── LoginPage.jsx  // 登录页面
    │   │   │   └── TaskPage.jsx   // 任务页面
    │   │   └── guard
    │   │       ├── 404.jsx        // 404页面
    │   │       └── guard.jsx      // 路由守卫
    │   └── shared
    │       ├── request.js         // 请求封装
    │       ├── stream.js          // websoket 流处理
    │       └── token.js           // Token 存储
    └── vite.config.js
```

### 后端结构

```
├── core
│   ├── bot                 // 腾讯频道机器人
│   │   ├── bot.go
│   │   ├── event.go
│   │   ├── messag.go
│   │   └── process.go
│   ├── captcha             // 验证码识别
│   │   ├── captcha.go
│   │   └── identify.go     // 识别接口
│   ├── chat
│   │   └── chat.go         // GPT聊天
│   ├── define
│   │   └── define.go       // 基础定义
│   ├── spider
│   │   ├── diary.go        // 实习填写
│   │   ├── simulate.go     // 实习日志
│   │   └── spider.go       // 自动化爬虫
│   └── util
│       ├── quene.go        // 队列
│       └── util.go         // 工具
├── demo.go
├── docker
│   ├── dockerfile
│   └── ocr
│       └── main.py         // 识别接口
├── go.mod
├── go.sum
├── main.go                 // 入口
└── res
    ├── img/                // 自动化存储的图片
    └── prompt              // 提示词
```

#### 启动

`前端`

```shell
cd frontend
npm i --registry=http://registry.npmmirror.com
npm run dev
```

`后端`

```shell
cd backend
go build . && ./could-work.exe
```
