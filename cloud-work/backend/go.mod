module could-work

go 1.21.0

require (
	github.com/Fromsko/gouitls v0.1.5
	github.com/Fromsko/rodPro v0.114.7
	github.com/electricbubble/go-toast v0.3.0
	github.com/panjf2000/ants/v2 v2.9.0
	github.com/sashabaranov/go-openai v1.17.9
	github.com/tencent-connect/botgo v0.1.6
	github.com/tidwall/gjson v1.17.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
)

require (
	github.com/fatih/color v1.15.0 // indirect
	github.com/go-resty/resty/v2 v2.10.0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/ysmood/fetchup v0.2.3 // indirect
	github.com/ysmood/goob v0.4.0 // indirect
	github.com/ysmood/got v0.34.1 // indirect
	github.com/ysmood/gson v0.7.3 // indirect
	github.com/ysmood/leakless v0.8.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
