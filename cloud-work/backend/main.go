package main

import (
	"could-work/core/define"
	"could-work/core/spider"
	"could-work/core/util"
	"time"

	rod "github.com/Fromsko/rodPro"
)

var log = define.Log

// RegisterWorkTask 注册工作任务
func RegisterWorkTask() {
	Web := spider.InitWeb(define.BaseUrl)

	if !Web.LoginPage() {
		log.Info("登录失败")
	} else {
		log.Info("登录成功")
	}

	Web.IndexPage(func(web *spider.Web, ele *rod.Element) error {
		web.SearchDiaryList(web.SearchLatestDiary(ele))

		handle := web.DiaryHandle(ele)

		util.MsgQueue.Push(&util.Message{
			Type: "receive",
			Data: util.H{
				"data": "111",
			},
		})

		handle.WaitUserReceive(func(dh spider.DiaryHandler) {
			dh["address"].MustInput("你好")
			dh["content"].MustInput("我也好")
			dh["upload"].MustSetFiles(define.ValidImg)
			// dh["submit"].MustClick() // TODO: 会提交日志
			time.Sleep(time.Second * 5)
			dh["cancel"].MustClick()
		})

		return nil
	})

	// time.Sleep(11 * time.Hour) // TODO: 调试

	defer func() {
		if r := recover(); r != nil {
			define.Log.Errorf("Recover %s", r)
			Web.Browser.MustClose()
		} else {
			define.Log.Info("关闭成功")
			Web.LogOutPage()
			Web.Browser.MustClose()
		}

		defer func() {
			util.Toask(util.H{
				"data":  "运行结束",
				"title": define.Title,
				"logo":  define.NotifyImg,
			})
		}()
	}()
}

func main() {
	// util.TaskRuner(
	// 	RegisterWorkTask,
	// )
	// fmt.Println(define.CONFIG.Proxy)
	// bot.RegisterBot(&define.CONFIG.QBot, "dev")
	RegisterWorkTask()
}
