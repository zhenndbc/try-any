package spider

import (
	"could-work/core/define"

	rod "github.com/Fromsko/rodPro"
)

var log = define.Log

type (
	// Web 自动化浏览器
	Web struct {
		Browser *rod.Browser
		Page    *rod.Page
	}
	// DiaryHandler 日志回调函数
	DiaryHandler map[string]*rod.Element
	// CallBack 回调函数
	CallBack func(web *Web, ele *rod.Element) error
)

// InitWeb 初始化浏览器
func InitWeb(URL string) *Web {
	w := &Web{
		Browser: rod.New().MustConnect(),
		Page:    nil,
	}
	w.Page = w.Browser.MustPage(URL)
	return w
}
