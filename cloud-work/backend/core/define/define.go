package define

import (
	"os"

	"could-work/core/util"
	"path/filepath"

	"github.com/Fromsko/gouitls/logs"
)

var (
	Log       = logs.InitLogger()
	CONFIG, _ = util.InitConfig()
	UserName  = CONFIG.Username
	PassWord  = CONFIG.Password
	BaseUrl   = CONFIG.HtmlURL.BaseUrl
	TokenUrl  = CONFIG.HtmlURL.TokenUrl
	CodeUrl   = CONFIG.HtmlURL.CodeUrl
)

var (
	WorkPath, _         = os.Getwd()
	PromptPath          = filepath.Join(WorkPath, "res", "prompt")
	ImgPath             = filepath.Join(WorkPath, "res", "img")
	NotifyImg           = filepath.Join(ImgPath, "气泡通知.png")
	LoginImg            = filepath.Join(ImgPath, "登录页.png")
	LogoutImg           = filepath.Join(ImgPath, "退出页")
	ValidImg            = filepath.Join(ImgPath, "验证码.png")
	IndexImg            = filepath.Join(ImgPath, "主页.png")
	InternshipNotice    = filepath.Join(ImgPath, "实习通知.png")
	InternshipJournal   = filepath.Join(ImgPath, "实习日志.png")
	InternshipDiaryList = filepath.Join(ImgPath, "%s实习日志.png")
)

var (
	Title   = "云就业平台"
	Version = "1.0"
)
