import time
import requests
import json


headers = {
    "authority": "qingk.com",
    "accept": "application/json, text/plain, */*",
    "accept-language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
    "cache-control": "no-cache",
    "content-type": "application/json",
    "expires": "0",
    "origin": "https://qingk.com",
    "platform": "web_pc",
    "pragma": "no-cache",
    "referer": "https://qingk.com/set/DHTFdld1OURm",
    "sec-ch-ua": "\"Not A(Brand\";v=\"99\", \"Microsoft Edge\";v=\"121\", \"Chromium\";v=\"121\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-origin",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 Edg/121.0.0.0"
}
cookies = {
    "csrfToken": "WPYjGhBITG3oUFp7Wa1tmiVl"
}
url = "https://qingk.com/api/dataview/open-data"
data = {
    "setId": "DHTFdld1OURm",
    "_req": time.time(),
}
data = json.dumps(data, separators=(',', ':'))
response = requests.post(url, headers=headers, cookies=cookies, data=data)

resp = response.json()

cardData = {'cards': {}}

if resp['code'] == 0:
    result = resp['result']
    if (cards := result.get('cards', )) is not None:
        for card in cards:
            content = card.get('frontContent', )
            content = json.loads(content)
            cardData['cards'].update({
                card["id"]: content[0]["insert"],
            })
            # print(f'''ID:=> {card["id"]}\ncontent:=> {content[0]["insert"]}''')

    if (cardSet := result.get('cardSet', )) is not None:
        title = cardSet['title']
        cardLen = len(cardSet['children'].split(','))
        cardData['title'] = title
        cardData['len'] = cardLen
        # print(f'title:=> {title}')
        # print(f'len:=> {cardLen}')

    if (author := result.get('author', )) is not None:
        photo = f"https://cdn.qingk.com/avatar/{author.get('photo', )}"
        cardData['photo'] = photo
        # print(f'photo:=> {photo}')
print(cardData)
