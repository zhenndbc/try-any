module note

go 1.21.0

require (
	github.com/Fromsko/gouitls v0.0.4
	github.com/tdewolff/parse/v2 v2.7.5
	github.com/tidwall/gjson v1.17.0
)

require (
	github.com/fatih/color v1.15.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
